package com.freewayso.image.combiner.enums;

/**
 * @Author zhaoqing.chen
 * @Date 2020/8/21
 * @Description 绘制方向
 **/

public enum Direction {
    /**
     * 左到右
     */
    LeftRight,
    /**
     * 右到左
     */
    RightLeft,
    /**
     * 中间到两边
     */
    CenterLeftRight
}
